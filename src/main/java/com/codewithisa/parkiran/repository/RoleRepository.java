package com.codewithisa.parkiran.repository;


import com.codewithisa.parkiran.model.enums.ERoles;
import com.codewithisa.parkiran.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(ERoles name);
}

